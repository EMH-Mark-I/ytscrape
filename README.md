# ytscrape

This is a bash script html scraper that can be used as an alternative to having a Google account for subscribing to YouTube channels. You can view a list of the most recent uploads across multiple channels from a single list within the terminal or a spreadsheet. The attempt of the script is to emulate the "Subscriptions" page from YouTube which aggregates a listing of video uploads from channels you are subscribed to.

Note: This is NOT a channel browser.

![](images/screenshot.png "screenshot")

#### Requirements

[youtube-dl](https://rg3.github.io/youtube-dl/)

[pup HTML parser](https://github.com/EricChiang/pup)

[mpv player](https://github.com/mpv-player/mpv/)

nano text editor

BASH

Linux

## Selections
[Known Issues.](KISSUES.md)

[Install Steps.](INSTALL.md)

[YTscrape Manual](MANUAL.md)

#### Credits

EMH-Mark-I
